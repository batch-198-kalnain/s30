db.fruits.aggregate([
	{$match:{$and:[{price:{$lt:50}},{supplier:"Yellow Farms"}]}},
	{$count:"yelloFarmFruitsWithPriceLessThan50"}
])

db.fruits.aggregate([
	{$match:{price:{$lt:30}}},
	{$count:"fruitsWithPriceLessThan30"},
])

db.fruits.aggregate([
	{$match:{supplier:"Yellow Farms"}},
	{$group:{_id:"$supplier",avgPrice:{$avg:"$price"}}}
])

db.fruits.aggregate([
	{$match:{supplier:"Red Farms Inc."}},
	{$group:{_id:"$supplier",maxPrice:{$max:"$price"}}}
])

db.fruits.aggregate([
	{$match:{supplier:"Red Farms Inc."}},
	{$group:{_id:"$supplier",minPrice:{$min:"$price"}}}
])