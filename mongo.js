// db.fruits.insertMany(
// 	[
// 		{
// 			name:"Apple",
// 			supplier:"Red Farms Inc.",
// 			stock:20,
// 			price:40,
// 			onSale:true,
// 		},
// 		{
// 			name:"Banana",
// 			supplier:"Yellow Farms",
// 			stock:15,
// 			price:20,
// 			onSale:true,
// 		},
// 		{
// 			name:"Kiwi",
// 			supplier:"Green Farming and Canning",
// 			stock:25,
// 			price:50,
// 			onSale:true,
// 		},
// 		{
// 			name:"Mango",
// 			supplier:"Yellow Farms",
// 			stock:10,
// 			price:60,
// 			onSale:true,
// 		},
// 		{
// 			name:"Dragon Fruit",
// 			supplier:"Red Farms Inc.",
// 			stock:10,
// 			price:60,
// 			onSale:true,
// 		}
// 	]
// )


  ///////////////////////////////////
 //  Aggregation Pipeline Stages  //
///////////////////////////////////
// Aggregation is typically done in 2-3 steps. Each process in aggregation is called a stage. 

// 1. $match - is used to match or get documents which satisfied the condition.
// syntax: {$match: {field:<value>}}

// 2. $group - allows us to group together documents and create an analysis out of the grouped.
	/* 
		_id: in the group stage, essentially associates an id to out results.
	 	_id: also determines the number of groups 
		_id: "$supplier" - essentially group together documents with the same values in the supplier field.
	*/

// db.fruits.aggregate([

// 	{$match: {onSale:true}},
// 	{$group: {_id:"$supplier",totalStocks:{$sum:"$stock"}}}

// ])

// db.fruits.aggregate([
// 	{$match: {onSale:true}},
// 	{$group: {_id:null,totalStocks:{$sum:"$stock"}}}
// ])

// db.fruits.updateOne(
// 	{"name":"Banana"},
//     {$set:{onSale:true}}
// )

// db.fruits.aggregate([
// 	{$match: {$and:[{onSale:true},{supplier:"Yellow Farms"}]}},
// 	{$group: {_id:"$supplier",totalStocks:{$sum:"$stock"}}}
// ])

// $avg - is an operator used in $group stage.
// $avg - gets the average of the numerical values of the indicated field in grouped documents

// ex.

// db.fruits.aggregate([

// 	{$match: {onSale:true}},
// 	{$group:{_id:"$supplier",avgStock:{$avg:"$stock"}}}

// ])
// db.fruits.aggregate([

// 	{$match:{onSale:true}},
// 	{$group:{_id:null,avgPrice:{$avg:"$price"}}}

// ])

//$max - will allows us to get the highest value out of all the values in a given field per group.
// ex.

// db.fruits.aggregate([
// 	{$match:{onSale:true}},
// 	{$group:{_id:"highestStockOnSale",maxStock:{$max:"$stock"}}}
// ])

// db.fruits.aggregate([

// 	{$match:{onSale:true}},
// 	{$group:{_id:null,maxPrice:{$max:"$price"}}}

// ])

// $min - get the lowest value of the values in a given field per group.
// ex.

// db.fruits.aggregate([

// 	{$match:{onSale:true}},
// 	{$group:{_id:"lowestStockOnSale",minStock:{$min:"$stock"}}}

// ])

// db.fruits.aggregate([

// 	{$match:{onSale:true}},
// 	{$group:{_id:"lowestPriceOnSale",minPrice:{$min:"$price"}}}

// ])

// db.fruits.aggregate([

// 	{$match:{price:{$lt:50}}},
// 	{$group:{_id:"lowestStock",minStock:{$min:"$stock"}}}

// ])


  //////////////////////
 //   Other Stages   //
//////////////////////

// $count - is a stage added after $match stage to count all items that matches our criteria.

// db.fruits.aggregate([

// 	{$match:{onSale:true}},
// 	{$count:"itemsOnSale"},
// ])

// db.fruits.aggregate([

// 	{$match:{price:{$lt:50}}},
// 	{$count:"itemsPriceLessThan50"},

// ])

// db.fruits.aggregate([

// 	{$match:{stock:{$lt:20}}},
// 	{$count:"forRestock"},

// ])

// $out - save/output the results in a new collection.
// Note: This will overwrite the collection if it already exists.
// ex.

// db.fruits.aggregate([

// 	{$match:{onSale:true}},
// 	{$group:{_id:"$supplier",totalStocks:{$sum:"$stock"}}},
// 	{$out:"stocksPerSupplier"}

// ])
